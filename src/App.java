import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        boolean inMatch = true;
        // Code to get players names
        String player1 = getPlayerName(1);
        String player2 = getPlayerName(2);

        while(inMatch) {
            // Creates a list of 9 positions
            String[] cases = new String[9];
            // At the beggining the 9 positions are empty
            for(int i=0; i< cases.length; i++) {
                cases[i] = " ";
            }
            boolean endGame = true;
            //Loop
            while(endGame) {
                assignSymbol(cases, player1, "X");
                if (getWinner(cases, "X")) {
                    System.out.print(player1);
                    break;
                } else {
                    assignSymbol(cases, player2, "O");
                    endGame = !getWinner(cases, "O");
                    if (endGame == false) {
                        System.out.print(player2);
                    }
                }
            }
            inMatch = replay();
        }
    }

    public static boolean replay() {
        boolean replay = true;
        String lettre = "y";
        System.out.println("\nvoulez-vous rejouer ? [y/n]");
        Scanner myObj = new Scanner(System.in);
        lettre = myObj.nextLine();
        if (lettre.equals("n")) {
            replay = false;
        }
        return replay;
    }
    //Functions that assigns a symbol to the list of 9 positions
    public static void assignSymbol(String[] cases, String player, String symb) {
        //symb = "X";
        int position = -1;
        createMap(cases);
        System.out.println("à ton tour " + player + " :");

        // Number verification
        boolean isNumber = true;
        while (isNumber) {
            try {
                Scanner myObj = new Scanner(System.in);
                position = myObj.nextInt();
                isNumber = false;
                if (position < 1 || position > 9) {
                    isNumber = true;
                    System.out.println("Commande incorrecte, veuillez sélectionner la position où vous voulez jouer " + player + ":");
                }
            } catch (InputMismatchException exception) {
                System.out.println("Commande incorrecte, veuillez sélectionner la position où vous voulez jouer " + player + ":");
            }
        }
        cases[position - 1] = symb;
    }

    public static String getPlayerName(int playerNumber) {
        // This function returns the name from the player's input
        System.out.println("Nom du joueur " + playerNumber + " ?");
        String userName = "Joueur NN";
        Scanner myObj = new Scanner(System.in);
        userName = myObj.nextLine();
        System.out.println("Your username is:" + userName);
        return userName;
    }

    public static boolean getWinner(String[] cases, String symb) {
        boolean winner = false;
        //String symb = "X";
        //System.out.println("Print all cases" + Arrays.toString(cases));
        if (cases[0].equals(symb) && cases[1].equals(symb) && cases[2].equals(symb)) {
            System.out.println("le gagnant est :");
            winner = true;
        } else if (cases[3].equals(symb) && cases[4].equals(symb) && cases[5].equals(symb)) {
            System.out.println("le gagnant est :");
            winner = true;
        } else if (cases[6].equals(symb) && cases[7].equals(symb) && cases[8].equals(symb)) {
            System.out.println("le gagnant est :");
            winner = true;
        }else if (cases[0].equals(symb) && cases[3].equals(symb) && cases[6].equals(symb)) {
            System.out.println("le gagnant est :");
            winner = true;
        }else if (cases[1].equals(symb) && cases[4].equals(symb) && cases[7].equals(symb)) {
            System.out.println("le gagnant est :");
            winner = true;
        }else if (cases[2].equals(symb) && cases[5].equals(symb) && cases[8].equals(symb)) {
            System.out.println("le gagnant est :");
            winner = true;
        }else if (cases[2].equals(symb) && cases[4].equals(symb) && cases[6].equals(symb)) {
            System.out.println("le gagnant est :");
            winner = true;
        }else if (cases[0].equals(symb) && cases[4].equals(symb) && cases[8].equals(symb)) {
            System.out.println("le gagnant est :");
            winner = true;
        }
        return winner;
    }

    public static void createMap(String[] cases) {
        System.out.print("Jeu:\t\t\tPosition:\n" +
                " - - -\t\t\t - - -\t\t\n" +
                "|" + cases[0] + "|" + cases[1] + "|" + cases[2] + "|\t\t\t|1|2|3|\n" +
                " - - -\t\t\t - - -\n" +
                "|" + cases[3] + "|" + cases[4] + "|" + cases[5] + "|\t\t\t|4|5|6|\n" +
                " - - -\t\t\t - - -\n" +
                "|" + cases[6] + "|" + cases[7] + "|" + cases[8] + "|\t\t\t|7|8|9|\n" +
                " - - -\t\t\t - - -\n");
    }
}
